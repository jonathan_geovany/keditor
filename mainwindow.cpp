#include "kodeeditor.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QHBoxLayout>
#include <QLabel>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    createActions();
    createMenus();
    //gradient style
    this->setStyleSheet("QMainWindow{background-color: black;}");


    ui->statusBar->setStyleSheet(""
                                 "background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(34, 34, 34, 255), stop:0.527094 rgba(44, 44, 44, 255), stop:1 rgba(54, 54, 54, 255));");
    //labels
    lblPosition     = new QLabel(this);
    lblLanguage     = new QLabel(this);
    lblLanguage->setStyleSheet("color : rgb(206, 206, 206);");
    lblPosition->setStyleSheet("color : rgb(206, 206, 206);");
    //adding labels to status bar
    ui->statusBar->addWidget(lblPosition);
    ui->statusBar->addPermanentWidget(lblLanguage);
    //load lenguages
    utils = new QJSONUtils("/languages.json");
    utils->loadLanguajes();
    //highlighter = new Highlighter(ui->plainTextEdit->document(), utils->getLanguajes().at(0));


    QVBoxLayout *splitterLayout = new QVBoxLayout;
    splitterLayout->setContentsMargins(0, 0, 0, 0);
    ui->centralWidget->setLayout(splitterLayout);
    splitterWidget = new SplitterWidget(this);
    tabWidget = splitterWidget->getTabWidget();
    splitterLayout->addWidget(splitterWidget);

    QModelIndex index =splitterWidget->getTreeView()->currentIndex();
    QFileInfo info = splitterWidget->getFileSystemModel()->fileInfo(index);

    connect(splitterWidget->getTabWidget(),SIGNAL(currentChanged(int)),this,SLOT(tabChanged(int)));
    connect(tabWidget,SIGNAL(tabCloseRequested(int)),this,SLOT(tabToClose(int)));

    isSideBarShow=true;
    showHideSideBar();

    loadLanguages();
    newEditor();
    newEditor();
}

void MainWindow::createActions()
{
    newAct = new QAction(tr("&New"), this);
    newAct->setShortcuts(QKeySequence::New);
    newAct->setStatusTip(tr("Create a new file"));
    connect(newAct, &QAction::triggered, this, &MainWindow::newFile);

    openAct = new QAction(tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, &QAction::triggered, this, &MainWindow::openFile);

    saveAct = new QAction(tr("&Save"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save the document"));
    connect(saveAct, &QAction::triggered, this, &MainWindow::saveFile);

    saveAsAct = new QAction(tr("SaveAs"), this);
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    saveAsAct->setStatusTip(tr("Save the document as .."));
    connect(saveAsAct, &QAction::triggered, this, &MainWindow::saveFileAs);

    closeAct = new QAction(tr("Close"), this);
    closeAct->setShortcuts(QKeySequence::Close);
    closeAct->setStatusTip(tr("Close the document"));
    connect(closeAct, &QAction::triggered, this, &MainWindow::closeFile);

    closeAllAct = new QAction(tr("Close All"), this);
    closeAllAct->setStatusTip(tr("Close the document"));
    connect(closeAllAct, &QAction::triggered, this, &MainWindow::closeAll);

    exitAct = new QAction(tr("Exit"), this);
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, &QAction::triggered, this, &MainWindow::exit);

    undoAct = new QAction(tr("&Undo"), this);
    undoAct->setShortcuts(QKeySequence::Undo);
    undoAct->setStatusTip(tr("Undo the last operation"));
    connect(undoAct, &QAction::triggered, this, &MainWindow::undo);

    redoAct = new QAction(tr("&Redo"), this);
    redoAct->setShortcuts(QKeySequence::Redo);
    redoAct->setStatusTip(tr("Redo the last operation"));
    connect(redoAct, &QAction::triggered, this, &MainWindow::redo);


    cutAct = new QAction(tr("Cu&t"), this);
    cutAct->setShortcuts(QKeySequence::Cut);
    cutAct->setStatusTip(tr("Cut the current selection's contents to the "
                            "clipboard"));
    connect(cutAct, &QAction::triggered, this, &MainWindow::cut);

    copyAct = new QAction(tr("&Copy"), this);
    copyAct->setShortcuts(QKeySequence::Copy);
    copyAct->setStatusTip(tr("Copy the current selection's contents to the "
                             "clipboard"));
    connect(copyAct, &QAction::triggered, this, &MainWindow::copy);

    pasteAct = new QAction(tr("&Paste"), this);
    pasteAct->setShortcuts(QKeySequence::Paste);
    pasteAct->setStatusTip(tr("Paste the clipboard's contents into the current "
                              "selection"));
    connect(pasteAct, &QAction::triggered, this, &MainWindow::paste);



    newSyntaxAct = new QAction(tr("New Syntax"), this);
    newSyntaxAct->setStatusTip(tr("Create syntax for a new language"));
    connect(newSyntaxAct, &QAction::triggered, this, &MainWindow::newSyntax);


    noneAct = new QAction(tr("None Syntax"), this);
    noneAct->setStatusTip(tr("Clear the syntax highlighting"));
    connect(noneAct, &QAction::triggered, this, &MainWindow::noneSyntax);

    minimizeAct = new QAction(tr("Minimize"), this);
    minimizeAct->setStatusTip(tr("Minimize the window"));
    connect(minimizeAct, &QAction::triggered, this, &MainWindow::minimizeWindow);

    sideBarAct = new QAction(tr("Show SideBar"), this);
    sideBarAct->setStatusTip(tr("Show or Hide SideBar"));
    connect(sideBarAct, &QAction::triggered, this, &MainWindow::showHideSideBar);


    nextDocAct = new QAction(tr("Next Document"), this);
    nextDocAct->setStatusTip(tr("Move to the next document"));
    connect(nextDocAct, &QAction::triggered, this, &MainWindow::nextDoc);

    previousDocAct = new QAction(tr("Previous Document"), this);
    previousDocAct->setStatusTip(tr("Move to the previous document"));
    connect(previousDocAct, &QAction::triggered, this, &MainWindow::previousDoc);


    aboutAct = new QAction(tr("&About kEditor"), this);
    aboutAct->setStatusTip(tr("Show the application's About box"));
    connect(aboutAct, &QAction::triggered, this, &MainWindow::about);

}
void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newAct);
    fileMenu->addAction(openAct);
    fileMenu->addAction(newAct);
    fileMenu->addAction(saveAct);
    fileMenu->addAction(saveAsAct);
    fileMenu->addSeparator();
    fileMenu->addAction(closeAct);
    fileMenu->addAction(closeAllAct);
    fileMenu->addAction(exitAct);

    //depuracion de aqui hacia abajo
    editMenu = menuBar()->addMenu(tr("&Edit"));
    editMenu->addAction(undoAct);
    editMenu->addAction(redoAct);
    editMenu->addSeparator();

    editMenu->addAction(cutAct);
    editMenu->addAction(copyAct);
    editMenu->addAction(pasteAct);

    syntaxMenu = menuBar()->addMenu(tr("&Syntax"));
    syntaxMenu->addAction(newSyntaxAct);
    syntaxMenu->addAction(noneAct);
    syntaxMenu->addSeparator();

    windowMenu = menuBar()->addMenu(tr("&Window"));
    windowMenu->addAction(minimizeAct);
    windowMenu->addAction(sideBarAct);
    windowMenu->addAction(previousDocAct);
    windowMenu->addAction(nextDocAct);

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);


}

void MainWindow::loadLanguages()
{

    QList<Language*> lstLanguages = utils->getLanguajes();
    qDebug() << utils->getLanguajes().size();
    foreach (Language* l, lstLanguages) {
        QAction* syntaxAct =  new QAction(l->getName(), this);
        //connect(syntaxAct, &QAction::triggered(), this, &MainWindow::selectSyntax);
        this->lstSyntaxAct.append(syntaxAct);
    }
    syntaxMenu->addActions(this->lstSyntaxAct);
    connect(syntaxMenu, SIGNAL(triggered(QAction*)), this, SLOT(selectSyntax(QAction*)));
}

void MainWindow::newEditor()
{
    KodeEditor* kEditor = new KodeEditor();
    kEditor->setDocumentTitle("Untitled");
    int p= tabWidget->addTab(kEditor,kEditor->documentTitle());
    tabWidget->setCurrentIndex(p);		//Set new current tab
    tabWidget->setCurrentWidget( kEditor);

    kEditor->setLanguage(utils->getLanguajes().at(0));
    //problemas con el kEditor
    lblLanguage->setText(kEditor->getLanguage()->getName());


    connect(kEditor,SIGNAL(cursorPositionChanged()),this,SLOT(cursorHasChanged()));
    connect(kEditor->document(), SIGNAL(contentsChanged()),this,SLOT(documentWasModified()));

    cursorHasChanged();

}

void MainWindow::cursorHasChanged()
{
    KodeEditor* kE= (KodeEditor*)tabWidget->currentWidget();
    if(!kE) return;
    QTextCursor cursor = kE->textCursor();
    QString position = " Line ";
    position+=QString::number(cursor.blockNumber()+1);
    position+=", Column ";
    position+=QString::number(cursor.columnNumber()+1);
    lblPosition->setText(position);
}

void MainWindow::newFile()
{
    newEditor();
}

void MainWindow::openFile()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFiles);
    QStringList fileNames;
    QString title="";
    int i=0;
    if (dialog.exec()){
        fileNames = dialog.selectedFiles();
        int tabIndex;
        for (i = 0; i < fileNames.length(); i++) {
            QFile file(fileNames.at(i));
            if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return;
            newFile();
            QTextStream in(&file);
            KodeEditor *editor = (KodeEditor*)tabWidget->currentWidget();
            editor->setExist(true);
            title = QFileInfo(fileNames.at(i)).fileName();
            editor->setPlainText(in.readAll());
            editor->setFileName(fileNames.at(i));
            editor->setDocumentTitle(title);
            tabWidget->setTabText(tabWidget->currentIndex(),editor->documentTitle());
            editor->setSaved(true);
        }
    }
}


void MainWindow::closeFile()
{
    int p = tabWidget->currentIndex();
    if(maybeSave(p)){
        tabWidget->removeTab(p);
    }
}

void MainWindow::closeAll()
{
    checkSaves();
}

void MainWindow::exit()
{

}

void MainWindow::undo()
{
    KodeEditor* kE= (KodeEditor*)tabWidget->currentWidget();
    if(kE!=NULL){
        kE->undo();
    }
}

void MainWindow::redo()
{
    KodeEditor* kE= (KodeEditor*)tabWidget->currentWidget();
    if(kE!=NULL){
        kE->redo();
    }
}

void MainWindow::cut()
{
    KodeEditor* kE= (KodeEditor*)tabWidget->currentWidget();
    if(kE!=NULL){
        kE->cut();
    }
}

void MainWindow::copy()
{
    KodeEditor* kE= (KodeEditor*)tabWidget->currentWidget();
    if(kE!=NULL){
        kE->copy();
    }
}

void MainWindow::paste()
{
    KodeEditor* kE= (KodeEditor*)tabWidget->currentWidget();
    if(kE!=NULL){
        kE->paste();
    }
}

void MainWindow::newSyntax()
{
    //solo test
    KodeEditor* kE= (KodeEditor*)tabWidget->currentWidget();
    if(kE!=NULL){
    kE->setLanguage(utils->getLanguajes().at(0));
    //problemas con el kEditor
    lblLanguage->setText(kE->getLanguage()->getName());
    }

}

void MainWindow::noneSyntax()
{
    KodeEditor* kE= (KodeEditor*)tabWidget->currentWidget();
    if(kE!=NULL){
        lblLanguage->setText("NONE");
        kE->removeHighlighter();

    }

}

void MainWindow::selectFont()
{

}

void MainWindow::minimizeWindow()
{
    this->showMinimized();
}

void MainWindow::showHideSideBar()
{
    QList<int> sizes;
    QString msg = (isSideBarShow)?"Show":"Hide";
    msg+=" SideBar";
    sideBarAct->setText(msg);
    sizes.append( (isSideBarShow)?0:170 );
    sizes.append(300);
    isSideBarShow = !isSideBarShow;
    splitterWidget->setSizes(sizes);
}

void MainWindow::previousDoc()
{
    int tabs    = tabWidget->children().size();
    int current = tabWidget->currentIndex();
    if(tabs>0 && current>0) current--;
    splitterWidget->getTabWidget()->setCurrentIndex(current);
}

void MainWindow::nextDoc()
{
    int tabs    = tabWidget->children().size();
    int current = tabWidget->currentIndex();
    if(current<=tabs) current++;
    splitterWidget->getTabWidget()->setCurrentIndex(current);
}

void MainWindow::about()
{

}

void MainWindow::selectSyntax(QAction* action)
{
    //qDebug() << i;
    qDebug() << "Triggered: " << action->text();
    for (int i=0;i<utils->getLanguajes().size();i++) {
        Language *l = utils->getLanguajes().at(i);
        if(action->text()==l->getName()){
            KodeEditor* kE= (KodeEditor*)tabWidget->currentWidget();
            if(kE!=NULL){
                kE->removeHighlighter();
                kE->setLanguage(l);
                lblLanguage->setText(l->getName());
            }
        }
    }

}

void MainWindow::tabChanged(int i)
{
    cursorHasChanged();
}

void MainWindow::tabToClose(int p){
    if(maybeSave(p)){
        tabWidget->removeTab(p);
    }
}


bool MainWindow::checkSaves(){
    for(int i=tabWidget->count()-1;i>=0;i--){
        tabWidget->setCurrentIndex(tabWidget->count()-1);
        KodeEditor *editor = (KodeEditor*)tabWidget->currentWidget();
        if(maybeSave(i)){
            tabWidget->removeTab(i);
        }else{
            break;
        }
    }
    if(tabWidget->count()==0)
        return true;
    else
        return false;
}

bool MainWindow::maybeSave(int index)
{

    KodeEditor *editor = (KodeEditor*)tabWidget->widget(index);
    QString msg = "The document ["+editor->documentTitle()+"] has been modified.\n"
                                                           "Do you want to save your changes?";
    if (editor->getSaved())
        return true;
    const QMessageBox::StandardButton ret
        = QMessageBox::warning(this, tr("CodeEditor"),
                               msg,
                               QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    switch (ret) {
    case QMessageBox::Save:
        saveFile();
        break;
    case QMessageBox::Cancel:
        return false;
    default:
        break;
    }
    return true;
}


void MainWindow::saveFile()
{
    KodeEditor *editor = (KodeEditor*)tabWidget->currentWidget();
    if(editor->getExist()){
        save(editor);
    }else{
        saveAs(editor);
    }
    tabWidget->setTabText(tabWidget->currentIndex(),editor->documentTitle());

}

void MainWindow::saveFileAs(){

    KodeEditor *editor = (KodeEditor*)tabWidget->currentWidget();
    saveAs(editor);
    tabWidget->setTabText(tabWidget->currentIndex(),editor->documentTitle());
}


void MainWindow::save(KodeEditor *e)
{
    QFile file(e->getFileName());
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) return;
    QTextStream out(&file);
    out << e->toPlainText();
    e->setExist(true);
    e->setSaved(true);
}

void MainWindow::saveAs(KodeEditor *e)
{
    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    if (dialog.exec() != QDialog::Accepted)
        return;
    e->setFileName(dialog.selectedFiles().first());
    e->setDocumentTitle(QFileInfo(dialog.selectedFiles().at(0)).fileName());
    save(e);
}

void MainWindow::documentWasModified()
{
    KodeEditor *editor = (KodeEditor*)tabWidget->currentWidget();
    if(editor){
        if(editor->isHasBeenModified()){
            tabWidget->setTabText(tabWidget->currentIndex(),editor->documentTitle()+"*");
            editor->setSaved(false);
        }
    }
}



MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    /*foreach(Highlighter* h,
            ui->plainTextEdit->findChildren<Highlighter*>()) {
        delete h;
        qDebug()<<"Eliminado";
    }
    highlighter = new Highlighter(ui->plainTextEdit->document(), utils->getLanguajes().at(1));
    */
}
