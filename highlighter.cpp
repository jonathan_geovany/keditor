#include "highlighter.h"
#include <QDebug>
#include <QRegularExpressionMatchIterator>

Highlighter::Highlighter(QTextDocument *parent, Language *language): QSyntaxHighlighter(parent)
{
    this->language = language;
    foreach(const Keyword &keyword,this->language->getKeywords()){
        HighlightingRule rule;
        foreach (const QString &pattern, keyword.keywordPatterns) {
            rule.pattern = QRegExp(pattern);
            rule.format = keyword.keywordFormat;
            highlightingRules.append(rule);
        }
    }
    highlightingRules.append(this->language->getHighlightingRules());
    commentStartExpression  = QRegExp(this->language->getBlockComment().start);
    commentEndExpression    = QRegExp(this->language->getBlockComment().end);
    multiLineCommentFormat  = this->language->getBlockCommentFormat();

    IsMatch.setFontStretch(10);
    IsMatch.setForeground(QColor(Qt::yellow));
    IsMatch.setBackground(QColor(Qt::green).lighter(60));

    IsNotMatch.setForeground(QColor(Qt::yellow));
    IsNotMatch.setBackground(QColor(Qt::red).lighter(60));

}

Language* Highlighter::getLanguage() const
{
    return language;
}

QList<QTextEdit::ExtraSelection> Highlighter::getBraceMatching(QTextCursor cursor)
{
    QList<QTextEdit::ExtraSelection> extraSelections;
    QTextEdit::ExtraSelection  selectionBracket;

    QTextCursor bracketBeginCursor;

    int position = cursor.position();
    int init=position;
    bool isValid=false;
    int i;
    for(i=0;i<SIMBOLS;i++){
        if ((!cursor.atBlockEnd()   && this->document()->characterAt(position) == openSymbols[i]) ||
            (!cursor.atBlockStart() && this->document()->characterAt(position - 1) == closeSymbols[i])){
            isValid=true;
            break;
        }
    }
    if(!isValid) return extraSelections;

    bool forward = this->document()->characterAt(position) == openSymbols[i];
    bool found=false;

    QTextCursor::MoveOperation move;
    QChar c, begin, end;


    if (forward) { //bool forward
       position++;
       move = QTextCursor::NextCharacter;
       begin = openSymbols[i];
       end   = closeSymbols[i];
    } else {
       position -= 2;
       init--;
       move = QTextCursor::PreviousCharacter;
       begin = closeSymbols[i];
       end   = openSymbols[i];
    }

    bracketBeginCursor = QTextCursor(cursor);
    bracketBeginCursor.movePosition(move, QTextCursor::KeepAnchor);
    this->stackOfSymbols[0]=0;
    this->stackOfSymbols[1]=0;
    this->stackOfSymbols[2]=0;
    this->stackOfSymbols[3]=0;


    this->stackOfSymbols[i] =1;
    int braceDepth = 1;

    while (!(c = this->document()->characterAt(position)).isNull())
    {
       if (c == begin) {
           braceDepth++;
       } else if (c == end) {
           braceDepth--;
       }
       if (checkCharacter(c,forward)) {//if braceDepth is zero
           selectionBracket.cursor = cursor;
           selectionBracket.cursor.setPosition(position);
           selectionBracket.cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor);
           selectionBracket.format = this->IsMatch;
           extraSelections.append(selectionBracket);
           found=true;
           break;
       }
       forward ? position++ : position--;
    }

    if(found){
       selectionBracket.format = this->IsMatch;
       selectionBracket.cursor = bracketBeginCursor;
       selectionBracket.cursor.setPosition(init);
       selectionBracket.cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor);
    }else{
       selectionBracket.format = this->IsNotMatch;
       selectionBracket.cursor = bracketBeginCursor;
       selectionBracket.cursor.setPosition(init);
       selectionBracket.cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor);
    }
    extraSelections.append(selectionBracket);
    return extraSelections;
}

void Highlighter::highlightBlock(const QString &text)
{
    foreach (const HighlightingRule &rule, highlightingRules) {
        QRegExp expression(rule.pattern);
        int index = expression.indexIn(text);
        while (index >= 0) {
            int length = expression.matchedLength();
            setFormat(index, length, rule.format);
            index = expression.indexIn(text, index + length);
        }
    }
    setCurrentBlockState(0);
    if(!validMultiLineComment(text)) return;
    int startIndex = 0;
    if (previousBlockState() != 1)
        startIndex = commentStartExpression.indexIn(text);

    while (startIndex >= 0) {
        int endIndex = commentEndExpression.indexIn(text, startIndex);
        int commentLength;
        if (endIndex == -1) {
            setCurrentBlockState(1);
            commentLength = text.length() - startIndex;
        } else {
            commentLength = endIndex - startIndex
                            + commentEndExpression.matchedLength();
        }
        setFormat(startIndex, commentLength, multiLineCommentFormat);
        startIndex = commentStartExpression.indexIn(text, startIndex + commentLength);
    }
}
bool Highlighter::validMultiLineComment(const QString &text)
{
    QRegExp start(this->language->getBlockComment().start);
    QRegExp end(this->language->getBlockComment().end);
    int s_index  = start.indexIn(text);
    int s_length = start.cap().length();
    int e_index  = end.indexIn(text,s_index);
    return (e_index-s_length>=0||e_index==-1)?true:false;
}

bool Highlighter::checkCharacter(QChar c,bool forward)
{
    //bajo revision
    QChar begin,end;
    for(int i=0;i<SIMBOLS;i++){
        if(forward){
            begin = openSymbols[i];
            end   = closeSymbols[i];
        }else{
            begin = closeSymbols[i];
            end   = openSymbols[i];
        }
        if (c == begin) {
            stackOfSymbols[i]++;
        } else if (c == end) {
            stackOfSymbols[i]--;
        }
    }
    return stackOfSymbols[0]==0 && stackOfSymbols[1]==0 && stackOfSymbols[2]==0 && stackOfSymbols[3]==0;
}
