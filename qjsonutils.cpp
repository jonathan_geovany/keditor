#include "qjsonutils.h"

QJSONUtils::QJSONUtils(QString _dir)
{
    this->dir=_dir;
}

bool QJSONUtils::loadLanguajes(QString _dir)
{
    if(_dir.length()>0) dir=_dir;

    QFile loadFile( QStringLiteral("/Volumes/HDD\ Mac/Data/Documents/Qt\ Projects/Kod-eEditor/languages.json"));

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open file.");
        return false;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    readLanguages(loadDoc.object());
    return true;
}

void QJSONUtils::readLanguages(const QJsonObject &json)
{
    languajes.clear();
    QJsonArray jsonLanguagesArray = json["languages"].toArray();
    for (int l = 0; l < jsonLanguagesArray.size(); ++l) {
        Language* languaje = new Language();
        languaje->read(jsonLanguagesArray[l].toObject());
        languajes.append(languaje);
    }
}


QList<Language *> QJSONUtils::getLanguajes() const
{
    return languajes;
}

void QJSONUtils::setLanguajes(const QList<Language *> &value)
{
    languajes = value;
}
