#ifndef HIGHLIGHTER_H
#define HIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QTextDocument>
#include <QTextEdit>
#include "language.h"

class Highlighter : public QSyntaxHighlighter
{
    Q_OBJECT
private:
    const static int SIMBOLS = 4;
    Language *language;
    QRegExp commentStartExpression;
    QRegExp commentEndExpression;
    QTextCharFormat multiLineCommentFormat;
    QList<HighlightingRule> highlightingRules;
    bool validMultiLineComment(const QString &text);
    QChar openSymbols [SIMBOLS]={'(','[','{','<'};
    QChar closeSymbols[SIMBOLS]={')',']','}','>'};
    int stackOfSymbols[SIMBOLS]={0,0,0,0};
    QTextCharFormat IsMatch;
    QTextCharFormat IsNotMatch;

    /*
     * ()
     *
     */
    bool checkCharacter(QChar c,bool forward);
public:
    Highlighter(QTextDocument *parent = 0,Language *language=0);
    Language* getLanguage() const;
    QList<QTextEdit::ExtraSelection> getBraceMatching(QTextCursor cursor);
protected:
    void highlightBlock(const QString &text) Q_DECL_OVERRIDE;

};

#endif // HIGHLIGHTER_H
