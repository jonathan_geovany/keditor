#ifndef ACTION_H
#define ACTION_H

#include <QAction>

class Action : public QAction
{
    Q_OBJECT
public:
    Action(QObject *parent = nullptr);
    Action(const QString &text, QObject *parent = nullptr);
};

#endif // ACTION_H
