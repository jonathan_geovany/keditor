#ifndef KODEEDITOR_H
#define KODEEDITOR_H

#include <QWidget>
#include <QPlainTextEdit>
#include <QPainter>

#include "highlighter.h"


class LineNumberArea;

class KodeEditor:public QPlainTextEdit
{
    Q_OBJECT

private:


    QString fileName;

    bool exist;
    bool saved;
    bool hasBeenModified;

    Highlighter *highlighter;
    Language *language;
    QWidget *lineNumberArea;
    QFont font;

    QString fontColor;
    QString backgroundColor;
    QString textSelectedColor;
    QString scrollAreaColor;
    QString scrollSliderColorHorizontal;
    QString scrollSliderColorVertical;
    void leftIndent();
    bool rightIndent();
public:
    KodeEditor(QWidget *parent = 0);
    void lineNumberAreaPaintEvent(QPaintEvent *event);
    int lineNumberAreaWidth();
    Language *getLanguage() const;
    void setLanguage(Language *value);
    void removeHighlighter();
    void setFontSize(int size);
    void updateStyle();
    void applyDefaultColors();
    bool getExist() const;
    void setExist(bool value);

    bool isHasBeenModified(){return hasBeenModified;}

    bool getSaved() const;
    void setSaved(bool value);

    QString getFileName() const;
    void setFileName(const QString &value);

protected:
    void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
    void keyPressEvent(QKeyEvent *e) Q_DECL_OVERRIDE;
    void focusInEvent(QFocusEvent *e) Q_DECL_OVERRIDE;
private slots:
    void updateLineNumberAreaWidth(int newBlockCount);
    void highlightCurrentLine();
    void highlightBraces();
    void hasBeenChanged();
    void updateLineNumberArea(const QRect &, int);
};



class LineNumberArea : public QWidget
{
    Q_OBJECT
private:
    KodeEditor *kEditor;
public:
    LineNumberArea(KodeEditor *editor): QWidget(editor) {
        kEditor = editor;
    }

    QSize sizeHint() const Q_DECL_OVERRIDE {
        return QSize(kEditor->lineNumberAreaWidth(), 0);
    }

protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE {
        kEditor->lineNumberAreaPaintEvent(event);
    }

};

#endif // KODEEDITOR_H
