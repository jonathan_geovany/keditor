﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QList>
#include <QMenu>
#include <QAction>
#include <QActionGroup>
#include <QFileDialog>
#include <QMessageBox>

#include "action.h"

#include "qjsonutils.h"
#include "highlighter.h"
#include "kodeeditor.h"
#include "splitterwidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    bool isSideBarShow;
    Ui::MainWindow *ui;
    QTabWidget *tabWidget;
    SplitterWidget *splitterWidget;
    Highlighter* highlighter;
    QJSONUtils* utils;
    QLabel *lblPosition;
    QLabel *lblLanguage;

    //to init menus
    void createActions();
    void createMenus();
    //menus available
    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *syntaxMenu;
    QMenu *formatMenu;
    QMenu *windowMenu;
    QMenu *helpMenu;

    QActionGroup *alignmentGroup;
    //actions to file menu
    QAction *newAct;
    QAction *openAct;
    QAction *saveAct;
    QAction *saveAsAct;
    QAction *closeAct;
    QAction *closeAllAct;
    QAction *exitAct;
    //actions to edit menu
    QAction *undoAct;
    QAction *redoAct;
    QAction *cutAct;
    QAction *copyAct;
    QAction *pasteAct;
    //actions to syntax menu
    QAction *newSyntaxAct;
    QAction *noneAct;
    //actions to format menu
    QAction *fontAct;
    //actions to windows mehu
    QAction *minimizeAct;
    QAction *sideBarAct;
    QAction *previousDocAct;
    QAction *nextDocAct;
    //actions to help menu
    QAction *aboutAct;
    QList<QAction*> lstSyntaxAct;

    //to load languajes
    void loadLanguages();

    //to handle the save
    bool maybeSave(int);
    void saveAs(KodeEditor*);
    void save(KodeEditor*);
    bool checkSaves();

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();
    void newEditor();
    void cursorHasChanged();
    void newFile();
    void openFile();
    void saveFile();
    void saveFileAs();
    void closeFile();
    void closeAll();
    void exit();
    void undo();
    void redo();
    void cut();
    void copy();
    void paste();
    void newSyntax();
    void noneSyntax();
    void selectFont();
    void minimizeWindow();
    void showHideSideBar();
    void previousDoc();
    void nextDoc();
    void about();

    void selectSyntax(QAction* action);

    void tabChanged(int i);
    void tabToClose(int i);
    void documentWasModified();

};

#endif // MAINWINDOW_H
