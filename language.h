#ifndef LANGUAGE_H
#define LANGUAGE_H

#include <QObject>
#include <QRegExp>
#include <QTextCharFormat>

#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

struct HighlightingRule
{
    QRegExp pattern;
    QTextCharFormat format;
    QString ruleName;
};
struct Keyword{
    QStringList keywordPatterns;
    QTextCharFormat keywordFormat;
};
struct MultiLineSymbol{
    QRegExp start;
    QRegExp end;
};

class Language
{
private:
    QString name;
    QList<HighlightingRule> highlightingRules;
    QList<Keyword> keywords;
    QList<MultiLineSymbol> foldingSymbols;
    QBrush getBrush(const QString input);
    QStringList getStringList(QJsonArray jsonArray);
    MultiLineSymbol blockComment;
    QTextCharFormat blockCommentFormat;
public:
    Language();
    Language(QString name);

    void addRule(HighlightingRule rule);
    void addKeyword(Keyword keyword);
    void addFoldingSymbol(MultiLineSymbol foldingSymbol);

    QList<HighlightingRule> getHighlightingRules();
    void setHighlightingRules(const QList<HighlightingRule> &value);
    QList<Keyword> getKeywords() const;
    void setKeywords(const QList<Keyword> &value);
    QList<MultiLineSymbol> getFoldingSymbols() const;
    void setFoldingSymbols(const QList<MultiLineSymbol> &value);
    QString getName() const;
    void setName(const QString &value);
    void read(const QJsonObject &json);
    void write(QJsonObject &json) const;
    void clearLanguage();
    MultiLineSymbol getBlockComment() const;
    QTextCharFormat getBlockCommentFormat() const;
};

#endif // LANGUAGE_H
