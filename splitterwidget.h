#ifndef SPLITTERWIDGET_H
#define SPLITTERWIDGET_H

#include <QWidget>
#include <QFileSystemModel>
#include <QSplitter>
#include <QTreeView>
#include <QItemSelectionModel>


class SplitterWidget : public QSplitter
{
    Q_OBJECT
public:
    explicit SplitterWidget(QWidget *parent = 0);
    void cdUp();
    QTabWidget *getTabWidget() const;
    QTreeView* getTreeView(){
        return treeView;
    }
    QFileSystemModel* getFileSystemModel(){return fileModel;}

private:
    QTreeView *treeView;
    QTabWidget *tabWidget;
    QFileSystemModel *fileModel;
    QItemSelectionModel* selectionModel;
signals:

public slots:
    void mySelectionChanged(const QItemSelection&,const QItemSelection&);
};

#endif // SPLITTERWIDGET_H
