#include "kodeeditor.h"

#include "QDebug"

KodeEditor::KodeEditor(QWidget *parent) : QPlainTextEdit(parent)
{
    font = QFont("Monaco", 12);
    font.setStyleHint(QFont::Monospace);
    font.setFixedPitch(true);

    exist=false;
    saved=true;

    setFont(font);
    setTabStopWidth(4 * fontMetrics().width(' '));

    lineNumberArea = new LineNumberArea(this);

    connect(this, SIGNAL(blockCountChanged(int)), this, SLOT(updateLineNumberAreaWidth(int)));
    connect(this, SIGNAL(updateRequest(QRect,int)), this, SLOT(updateLineNumberArea(QRect,int)));
    connect(this, SIGNAL(cursorPositionChanged()), this, SLOT(highlightBraces()));
    connect(this,SIGNAL(textChanged()),this,SLOT(hasBeenChanged()));
    updateLineNumberAreaWidth(0);


    //default colors value

    setLineWrapMode(QPlainTextEdit::NoWrap);
    this->applyDefaultColors();
    this->updateStyle();
    setTabChangesFocus(false);
    hasBeenModified=false;
}

Language *KodeEditor::getLanguage() const
{
    return language;
}

void KodeEditor::setLanguage(Language *value)
{
    removeHighlighter();
    language = value;
    highlighter = new Highlighter(this->document(),language);
}

void KodeEditor::removeHighlighter()
{


    QTextCursor cursor = textCursor();
    cursor.movePosition(QTextCursor::Start, QTextCursor::MoveAnchor,1);
    setTextCursor(cursor);
    clearFocus();
    highlighter = NULL;
    foreach(Highlighter* h,this->findChildren<Highlighter*>()) {
        delete h;
    }



}


int KodeEditor::lineNumberAreaWidth()
{
    int digits = 1;
    int max = qMax(1, blockCount());
    while (max >= 10) {
        max /= 10;
        ++digits;
    }
    int space = 18 + fontMetrics().width(QLatin1Char('9')) * digits;
    return space;
}

void KodeEditor::setFontSize(int size)
{
    font.setPointSize(size);
    setFont(font);
    setTabStopWidth(4 * fontMetrics().width(' '));
}

void KodeEditor::updateStyle()
{
    this->setStyleSheet("QPlainTextEdit {color: "+fontColor+"; border-style: outset;"
                        "border-radius: 2px;"
                        "background-color: "+backgroundColor+" ;"
                        "selection-color: "+fontColor+";"
                        "selection-background-color: "+textSelectedColor+";}"
                        "QScrollBar:vertical {"
                        "    border-style: none;"
                        "    background: "+scrollAreaColor+" ;"
                        "    width: 16px;"
                        "    margin: 0px;"
                        "}"
                        "QScrollBar::handle:vertical {"
                        "   background: "+scrollSliderColorVertical+";"
                        "   min-height: 16px;"
                        "   margin-left: 3px;"
                        "   margin-right: 3px;"
                        "   border-style: outset;"
                        "   border-radius: 5px;"
                        "}"
                        "QScrollBar::add-line:vertical { background: "+scrollAreaColor+" ; }"
                        "QScrollBar::sub-line:vertical { background: "+scrollAreaColor+" ; }"
                        "QScrollBar:horizontal {"
                        "    border-style: none;"
                        "    background: "+scrollAreaColor+";"
                        "    height: 16px;"
                        "    margin: 0px;"
                        "}"
                        "QScrollBar::handle:horizontal {"
                        "   background: "+scrollSliderColorHorizontal+";"
                        "   min-width: 16px;"
                        "   margin-top: 3px;"
                        "   margin-bottom: 3px;"
                        "   border-style: outset;"
                        "   border-radius: 5px;"
                        "}"
                        "QScrollBar::add-line:horizontal { background: "+scrollAreaColor+" ; }"
                        "QScrollBar::sub-line:horizontal { background: "+scrollAreaColor+" ; }"
                        "QAbstractScrollArea::corner {"
                        "background: "+scrollAreaColor+";"
                        "}"
                        );
}

void KodeEditor::applyDefaultColors()
{
    fontColor           ="rgb(248, 248, 248)";
    backgroundColor     ="rgb(38, 40, 36)";
    textSelectedColor   ="rgba(75, 75, 75, 75%)";
    scrollAreaColor     ="rgb(34, 34, 34)";
    scrollSliderColorVertical     ="qlineargradient(spread:reflect, x1:1, y1:0.5, x2:0, y2:0.5, stop:0.00254453 rgba(60, 60, 60, 255), stop:0.5 rgba(65, 65, 65, 255), stop:1 rgba(60, 60, 60, 255))";
    scrollSliderColorHorizontal   ="qlineargradient(spread:reflect, x1:0, y1:0, x2:0, y2:1, stop:0.00254453 rgba(60, 60, 60, 255), stop:0.5 rgba(65, 65, 65, 255), stop:1 rgba(60, 60, 60, 255))";
}


void KodeEditor::updateLineNumberAreaWidth(int /* newBlockCount */)
{
    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}


void KodeEditor::updateLineNumberArea(const QRect &rect, int dy)
{
    if (dy)
        lineNumberArea->scroll(0, dy);
    else
        lineNumberArea->update(0, rect.y(), lineNumberArea->width(), rect.height());

    if (rect.contains(viewport()->rect()))
        updateLineNumberAreaWidth(0);
}


void KodeEditor::resizeEvent(QResizeEvent *e)
{
    QPlainTextEdit::resizeEvent(e);

    QRect cr = contentsRect();
    lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}

void KodeEditor::keyPressEvent(QKeyEvent *e)
{
    switch(e->key())
    {
    case Qt::Key_Tab:
        if(rightIndent())return;
        break;
    case Qt::Key_Backtab:
        leftIndent();
        break;
    }



    QPlainTextEdit::keyPressEvent(e);

}

void KodeEditor::focusInEvent(QFocusEvent *e)
{
    QPlainTextEdit::focusInEvent(e);
}

void KodeEditor::highlightCurrentLine()
{
    QList<QTextEdit::ExtraSelection> extraSelections;

    if (!isReadOnly()) {
        QTextEdit::ExtraSelection selection;
        QColor lineColor = QColor(Qt::cyan).lighter(20);

        selection.format.setBackground(lineColor);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = textCursor();
        selection.cursor.clearSelection();
        extraSelections.append(selection);
    }
    setExtraSelections(extraSelections);
}

void KodeEditor::highlightBraces()
{
    QList<QTextEdit::ExtraSelection> extraSelections;
    if(highlighter){
        QTextCursor cur = textCursor();
        extraSelections.append( highlighter->getBraceMatching(textCursor()) );

        /*


        QString prev="",next="";
        if(cur.movePosition(QTextCursor::PreviousCharacter)){
            cur.movePosition(QTextCursor::NextCharacter,QTextCursor::KeepAnchor);
            prev = cur.selectedText();
            cur.clearSelection();
         }
        cur=textCursor();
        if(cur.movePosition(QTextCursor::NextCharacter,QTextCursor::KeepAnchor)){
            next = cur.selectedText();
            //end=false;
        }else{
            //end=true;
        }
        QTextCursor bracketBeginCursor,bracketEndCursor;
        QTextCursor cursor = textCursor();


        QTextEdit::ExtraSelection  selectionBracket;
        QColor bracketMismatchFormat,bracketMatchFormat;
        bracketMatchFormat = QColor(Qt::green).lighter(57);
        bracketMismatchFormat = QColor(Qt::red).lighter(57);

        int position = cursor.position();
        int temp_=position;

           if ((!cursor.atBlockEnd()   && this->document()->characterAt(position) == '(') ||
               (!cursor.atBlockStart() && this->document()->characterAt(position - 1) == ')')) {

               bool forward = this->document()->characterAt(position) == '(';
               bool found=false;

               QTextCursor::MoveOperation move;

               QChar c, begin, end;

               if (forward) { //bool forward =
                   position++;
                   move = QTextCursor::NextCharacter;
                   begin = '(';
                   end   = ')';

               } else {
                   position -= 2;
                   temp_--;
                   move = QTextCursor::PreviousCharacter;
                   begin = ')';
                   end   = '(';
               }

               bracketBeginCursor = QTextCursor(cursor);
               bracketBeginCursor.movePosition(move, QTextCursor::KeepAnchor);

               QColor format = bracketMismatchFormat;

               int braceDepth = 1;

               while (!(c = this->document()->characterAt(position)).isNull())
               {
                   if (c == begin) {
                       braceDepth++;
                   } else if (c == end) {
                       braceDepth--;

                       if (!braceDepth) {
                           format = bracketMatchFormat;
                           selectionBracket.cursor = textCursor();
                           selectionBracket.cursor.setPosition(position);
                           selectionBracket.cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor);
                           selectionBracket.format.setBackground(format);
                           extraSelections.append(selectionBracket);
                           found=true;
                           break;
                       }
                   }
                   forward ? position++ : position--;
               }
               if(found){

                   selectionBracket.format.setBackground(format);
                   selectionBracket.cursor = bracketBeginCursor;
                   selectionBracket.cursor.setPosition(temp_);
                   selectionBracket.cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor);
               }else{
                   selectionBracket.format.setBackground(bracketMismatchFormat);
                   selectionBracket.cursor = bracketBeginCursor;
                   selectionBracket.cursor.setPosition(temp_);
                   selectionBracket.cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor);
               }

               extraSelections.append(selectionBracket);
           }*/

           setExtraSelections(extraSelections);
    }

}

void KodeEditor::hasBeenChanged()
{
    if(!hasBeenModified && this->document()->isEmpty())
        return;
    hasBeenModified=true;
}

void KodeEditor::lineNumberAreaPaintEvent(QPaintEvent *event)
{
    QPainter painter(lineNumberArea);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    painter.fillRect(event->rect(), QColor("#262824"));
    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
    int bottom = top + (int) blockBoundingRect(block).height();
    painter.setPen(Qt::gray);
    painter.setFont(font);
    while (block.isValid() && top <= event->rect().bottom()) {
        if (block.isVisible() && bottom >= event->rect().top()) {
            QString number = QString::number(blockNumber + 1);
            painter.drawText(0, top, lineNumberArea->width() - 8, fontMetrics().height(),
                             Qt::AlignRight, number);
        }
        block = block.next();
        top = bottom;
        bottom = top + (int) blockBoundingRect(block).height();
        ++blockNumber;
    }
    block = textCursor().block();
    if(block.isValid()){
        blockNumber = block.blockNumber();
        top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
        QColor color = QColor("#787878");
        color.setAlpha(100);
        painter.fillRect(0,top,lineNumberArea->width(),fontMetrics().height(),color );
    }
}
bool KodeEditor::getExist() const
{
    return exist;
}

void KodeEditor::setExist(bool value)
{
    exist = value;
}

bool KodeEditor::getSaved() const
{
    return saved;
}

void KodeEditor::setSaved(bool value)
{
    saved = value;
}

QString KodeEditor::getFileName() const
{
    return fileName;
}

void KodeEditor::setFileName(const QString &value)
{
    fileName = value;
}

void KodeEditor::leftIndent()
{
    QTextCursor cur = textCursor();
    int a = cur.anchor();
    int p = cur.position();
    int start = (a<=p?a:p);
    int end = (a>p?a:p);

    cur.beginEditBlock();
    cur.setPosition(end);
    int eblock = cur.block().blockNumber();
    cur.setPosition(start);
    int sblock = cur.block().blockNumber();
    QString s;
    for(int i = sblock; i <= eblock; i++)
    {
        cur.movePosition(QTextCursor::EndOfBlock, QTextCursor::MoveAnchor);
        cur.movePosition(QTextCursor::StartOfBlock, QTextCursor::KeepAnchor);
        s = cur.selectedText();
        if(!s.isEmpty()){
            if(s.startsWith("    ") || s.startsWith("   \t")){
                cur.movePosition(QTextCursor::StartOfBlock, QTextCursor::MoveAnchor);
                cur.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor, 4);
                cur.removeSelectedText();
            }else if(s.startsWith("   ") || s.startsWith("  \t")){
                cur.movePosition(QTextCursor::StartOfBlock, QTextCursor::MoveAnchor);
                cur.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor, 3);
                cur.removeSelectedText();
            }else if(s.startsWith("  ") || s.startsWith(" \t")){
                cur.movePosition(QTextCursor::StartOfBlock, QTextCursor::MoveAnchor);
                cur.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor, 2);
                cur.removeSelectedText();
            }else if(s.startsWith(" ") || s.startsWith("\t")){
                cur.movePosition(QTextCursor::StartOfBlock, QTextCursor::MoveAnchor);
                cur.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor, 1);
                cur.removeSelectedText();
            }
        }
        cur.movePosition(QTextCursor::NextBlock, QTextCursor::MoveAnchor);
    }
    cur.endEditBlock();
}

bool KodeEditor::rightIndent()
{
    QTextCursor curs = textCursor();
    if(!curs.hasSelection()){
        curs.insertText("    ");
        setTextCursor(curs);
        return true;
    }
    int spos = curs.anchor(), epos = curs.position();
    if(spos > epos)
    {
        int hold = spos;
        spos = epos;
        epos = hold;
    }
    curs.setPosition(spos, QTextCursor::MoveAnchor);
    int sblock = curs.block().blockNumber();
    curs.setPosition(epos, QTextCursor::MoveAnchor);
    int eblock = curs.block().blockNumber();
    curs.setPosition(spos, QTextCursor::MoveAnchor);
    curs.beginEditBlock();
    for(int i = 0; i <= (eblock - sblock); ++i)
    {
        curs.movePosition(QTextCursor::StartOfBlock, QTextCursor::MoveAnchor);
        curs.insertText("    ");
        curs.movePosition(QTextCursor::NextBlock, QTextCursor::MoveAnchor);
    }
    curs.endEditBlock();
    curs.setPosition(spos, QTextCursor::MoveAnchor);
    curs.movePosition(QTextCursor::StartOfBlock, QTextCursor::MoveAnchor);
    while(curs.block().blockNumber() < eblock)
    {
        curs.movePosition(QTextCursor::NextBlock, QTextCursor::KeepAnchor);
    }
    curs.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
    setTextCursor(curs);
    return true;
}




