#ifndef QJSON_H
#define QJSON_H

#include <QObject>
#include <QFile>

#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

#include "language.h"
class QJSONUtils
{
private:
    QList<Language *> languajes;
    QString dir;
public:
    QJSONUtils(QString _dir);
    bool loadLanguajes(QString _dir="");
    bool saveLanguajes(QString _dir="");
    void readLanguages(const QJsonObject &json);
    void write(QJsonObject &json) const;
    QList<Language *> getLanguajes() const;
    void setLanguajes(const QList<Language *> &value);
};

#endif // QJSON_H
