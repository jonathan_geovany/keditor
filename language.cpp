#include "language.h"

#include "QDebug"

Language::Language()
{
    this->name="No name";
}
Language::Language(QString name)
{
    this->name=name;
}
void Language::addRule(HighlightingRule rule)
{
    highlightingRules.append(rule);
}
void Language::addKeyword(Keyword keyword)
{
    keywords.append(keyword);
}
void Language::addFoldingSymbol(MultiLineSymbol foldingSymbol)
{
    foldingSymbols.append(foldingSymbol);
}
QList<HighlightingRule> Language::getHighlightingRules()
{
    return highlightingRules;
}
void Language::setHighlightingRules(const QList<HighlightingRule> &value)
{
    highlightingRules = value;
}
QList<Keyword> Language::getKeywords() const
{
    return keywords;
}
void Language::setKeywords(const QList<Keyword> &value)
{
    keywords = value;
}
QList<MultiLineSymbol> Language::getFoldingSymbols() const
{
    return foldingSymbols;
}
void Language::setFoldingSymbols(const QList<MultiLineSymbol> &value)
{
    foldingSymbols = value;
}
QString Language::getName() const
{
    return name;
}
void Language::setName(const QString &value)
{
    name = value;
}

void Language::read(const QJsonObject &json)
{
    clearLanguage();
    name = json["name"].toString();
    qDebug() << "\n=>Language : " << name;
    QJsonArray rulesArray = json["rules"].toArray();
    for (int i = 0; i < rulesArray.size(); ++i) {
        QJsonObject jsonRule = rulesArray[i].toObject();
        HighlightingRule rule;
        rule.format.setForeground(getBrush(jsonRule["format"].toString()));
        rule.pattern  = QRegExp(jsonRule["pattern"].toString());
        rule.ruleName = jsonRule["ruleName"].toString();
        qDebug() << "==>ruleName : " << rule.ruleName;
        qDebug() << "==>pattern : " << jsonRule["pattern"].toString();
        qDebug() << "==>format : " << jsonRule["format"].toString();

        addRule(rule);
    }



    QJsonArray keywordsArray = json["keywords"].toArray();
    for (int i = 0; i < keywordsArray.size(); ++i) {
        QJsonObject jsonKeyword = keywordsArray[i].toObject();
        Keyword keyword;
        keyword.keywordFormat.setForeground(getBrush(jsonKeyword["keywordFormat"].toString()));
        keyword.keywordPatterns = getStringList(jsonKeyword["keywordPatterns"].toArray());
        qDebug() << "==>keywordFormat : " << jsonKeyword["keywordFormat"].toString();
        qDebug() << "==>keywordPatterns : " << keyword.keywordPatterns;
        addKeyword(keyword);
    }

    QJsonArray foldingSymbolsArray = json["foldingSymbols"].toArray();
    for (int i = 0; i < foldingSymbolsArray.size(); ++i) {
        QJsonObject jsonfoldingSymbol = foldingSymbolsArray[i].toObject();
        MultiLineSymbol foldingSymbol;
        foldingSymbol.start = QRegExp(jsonfoldingSymbol["start"].toString());
        foldingSymbol.end   = QRegExp(jsonfoldingSymbol["end"].toString());
        qDebug() << "==>folding start : " << jsonfoldingSymbol["start"].toString();
        qDebug() << "==>folding end : " << jsonfoldingSymbol["end"].toString();
    }
    QJsonObject jsonBlockComment = json["blockComment"].toObject();
    this->blockComment.start = QRegExp(jsonBlockComment["start"].toString() );
    this->blockComment.end = QRegExp(jsonBlockComment["end"].toString() );
    this->blockCommentFormat.setForeground(getBrush(jsonBlockComment["format"].toString()));
    qDebug() << "==>commentStart : " << jsonBlockComment["start"].toString();
    qDebug() << "==>commentEnd   : " << jsonBlockComment["end"].toString();
    qDebug() << "==>commentFormat: " << jsonBlockComment["format"].toString();
}

void Language::write(QJsonObject &json) const
{
    //json["name"] = name;
}

void Language::clearLanguage()
{
    highlightingRules.clear();
    keywords.clear();
    foldingSymbols.clear();
    name="no name";

}
QBrush Language::getBrush(const QString input)
{
    QStringList colors = input.split(",");
    QString r=colors[0],g=colors[1],b=colors[2];
    return QBrush(QColor(r.toInt(),g.toInt(),b.toInt()));
}

QStringList Language::getStringList(QJsonArray jsonArray)
{
    QStringList list;
    for (int i = 0; i < jsonArray.size(); ++i) {
        list.append(jsonArray[i].toString());
    }
    return list;
}

QTextCharFormat Language::getBlockCommentFormat() const
{
    return blockCommentFormat;
}

MultiLineSymbol Language::getBlockComment() const
{
    return blockComment;
}

