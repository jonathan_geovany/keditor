#include "splitterwidget.h"

#include <QDebug>

SplitterWidget::SplitterWidget(QWidget *parent) : QSplitter(parent)
{

    setStyleSheet("QSplitter::handle{"
                  "background: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0.00985222 rgba(38, 40, 38, 255), stop:0.3 rgba(70, 72, 70, 255), stop:0.7 rgba(70, 72, 70, 255), stop:1 rgba(38, 40, 38, 255));"
                  "}"
                  );
    this->setFocusPolicy(Qt::NoFocus);
    treeView = new QTreeView;
    treeView->setFocusPolicy(Qt::NoFocus);
    fileModel = new QFileSystemModel(this);
    fileModel->setRootPath(QDir::currentPath());
    treeView->setModel(fileModel);

    treeView->setRootIndex(fileModel->index(QDir::currentPath()));
    treeView->setColumnWidth(0, 200);
    treeView->setColumnHidden(0,false);
    treeView->setColumnHidden(1,true);
    treeView->setColumnHidden(2,true);
    treeView->setColumnHidden(3,true);
    tabWidget = new QTabWidget;
    tabWidget->setStyleSheet(
                             "QTabWidget::pane {"
                             "border-top: 1px solid #262824;"
                             "border-color: #3b3d38;"
                             "}"
                             "QTabWidget::tab-bar {"
                             "left: 0px;"
                             "alignment: left;"
                             "}"
                             "QTabBar::tab {"
                             "background: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, stop:0 rgba(68, 70, 66, 255), stop:0.55 rgba(70, 72, 70, 255), stop:1 rgba(92, 95, 92, 255));"
                             "border-top-left-radius: 8px;"
                             "border-top-right-radius: 8px;"
                             "min-width: 8ex;"
                             "padding: 3px;"
                             "border-bottom: none;"
                             "}"
                             "QTabBar::tab:selected {"
                             "background: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, stop:0 rgba(38, 40, 36, 255), stop:0.55 rgba(40, 42, 40, 255), stop:1 rgba(62, 65, 62, 255));"
                             "color: #e0e0e0;"
                             "border: 2px solid #3b3d38;"
                             "border-bottom: none;"
                             "}"
                             "QTabBar::tab:!selected {"
                             "border: 1px solid #3b3d38;"
                             "margin-top: 3px;"
                             "color: #aaaaaa;"
                             "border-bottom: none;"
                             "}"
                             );
    tabWidget->setTabsClosable(true);
    addWidget(treeView);
    addWidget(tabWidget);

    setStretchFactor(1, 1);

    setCollapsible(0,true);
    setCollapsible(1,true);
    selectionModel = treeView->selectionModel();
    connect(selectionModel, SIGNAL(selectionChanged(const QItemSelection&,const QItemSelection&)), this, SLOT(mySelectionChanged(const QItemSelection&,const QItemSelection&)));
}


void SplitterWidget::mySelectionChanged(const QItemSelection&,const QItemSelection&){
    qDebug() << "nueva ruta ";
}

void SplitterWidget::cdUp()
{
    QDir dir = fileModel->rootDirectory();
    dir.cdUp();
    fileModel->setRootPath(dir.absolutePath());
    treeView->setRootIndex(fileModel->index(dir.absolutePath()));
    qDebug() << dir.absolutePath();
}

QTabWidget *SplitterWidget::getTabWidget() const
{
    return tabWidget;
}
